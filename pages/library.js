import P from '../components/paragraph'
import Post from '../components/post'
import ListItem from '../components/listItem'

const list = [{
  id: 1,
  label: 'Button',
  componentType: 'CustomButton'
},
{
  id: 2,
  label: 'Paragraph',
  componentType: 'Paragraph'
}]

export default () => (
  <div className="main">
    <div className="leftContainer">
      {list.map(item => {
        return (<ListItem
          title={item.label}
          onPress={() => console.log("test")} />)
      })}
    </div>
    <div className="leftContainer">
      <Post title="My second blog post">
        <P>Hello there</P>
        <P>This is another example.</P>
        <P>Wa-hoo!</P>
      </Post>

    </div>
    <style jsx>{`
      .main {
        columns: 2 auto;
      }

      .leftContainer: {
          flex:1
      }
      hr {
        width: 100px;
        border-width: 0;
        margin: 20px auto;
        text-align: center;
      }

      hr::before {
        content: '***';
        color: #ccc;
      }
    `}</style>
  </div>
)
